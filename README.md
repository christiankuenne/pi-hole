# Docker Pi-hole and Unbound for Raspberry Pi

My docker-compose for Pi-hole and Unbound.

## Getting started

Configure your settings in [.env](.env). Conditional Forwarding is enabled by default. That means: DNS queries for local devices will be sent to the router. Set your Pi-hole's IP address, the Local Network CIDR and the router's IP address.

### Start the containers:

```
$ make start
```

### Login to the console:

Open http://pi.hole/admin. Go to _Settings > Privacy_ and select _Anonymous Mode_. This is necessary eventhough the environment variable `QUERY_LOGGING` is already set to `false`.

## Set Unbound as Pi-hole's DNS Server

**Edit: In a previous version of Pi-hole, it was not possible to set the DNS Server in docker-compose using the Docker service name. This has been fixed, so this section can be ignored.**

Unbound is only used from Pi-hole, so it's not necessary to expose any ports.

I tried to set the following environment variable in my docker-compose.yml:

```
environment:
    - PIHOLE_DNS_: unbound#53
```

Unfortunately that's not working. Pi-hole gives out that the container name is not a valid IP address. As a workaround, we need to find Unbound's container IP address.

List all running containers to get Pi-hole's container ID:

```
$ docker container ls
```

Run bash inside the Pi-hole container:

```
$ docker exec -it [CONTAINER_ID] /bin/bash
```

Ping Unbound to get its IP address:

```
$ ping unbound
```

We can also test Unbound here:

```
$ dig spiegel.de @unbound -p 53
```

Login to the Pi-hole console `(http://SERVER/admin)` and open **Settings > DNS**. Under **Upstream DNS Servers** untick Google and add Unbound's IP address under **Custom 1 (IPv4)**. Let's say Unbound's address is 172.18.0.2:

```
172.18.0.2#53
```

Click **Save**. Back in the Pi-hole container, we can now test to make sure that Pi-hole is really using unbound:

```
$ dig spiegel.de @localhost
```

If query logging is enabled, we should see the request in the logs.

## Change Pi-hole password

Run bash inside the Pi-hole container and enter:

```
$ pihole -a -p
```

## Configure DNS Server on FRITZ!Box

Open **Heimnetz > Netzwerk > Netzwerkeinstellungen**. Expand **Weitere Einstellungen**. Open **IPv4-Einstellungen**. Under **Lokaler DNS-Server**, enter Pi-hole's IP address. Restart the router.

## Issue: High CPU Usage on Version 2022.04

In version 2022.04, the base image has been updated from _buster-slim_ to _bullseye-slim_. The new image requires _libseccomp2_ version 2.5. The version on Raspbian Buster is 2.3. The issue is described on [GitHub](https://github.com/pi-hole/docker-pi-hole/issues/1042).

Check your version of _libseccomp2_:

```
$ apt list libseccomp2 -a
```

If version 2.5 is not installed, you can install it from backports. Instructions from [linuxserver.io](https://docs.linuxserver.io/faq):

```
$ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 04EE7237B7D453EC 648ACFD622F3D138
$ echo "deb http://deb.debian.org/debian buster-backports main" | sudo tee -a /etc/apt/sources.list.d/buster-backports.list
$ sudo apt update
% sudo apt install -t buster-backports libseccomp2
```
